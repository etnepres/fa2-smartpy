Generated Michelson Contracts
=============================


Latest builds as pure Michelson files:

* `20200910-203659+0000_5060996_contract.tz`: The default.
* `20200910-203659+0000_5060996_dbg_contract.tz`: The default in debug mode.
* `20200910-203659+0000_5060996_mutran_contract.tz`: The default with mutez transfer entry-point.
* `20200910-203659+0000_5060996_perdesc_noops_contract.tz`: The default without operators and with permissions-descriptor.
* `20200910-203659+0000_5060996_single_mutran_contract.tz`: The single-asset with mutez transfer entry-point.
* `20200910-203659+0000_5060996_nft_mutran_contract.tz`: The default in NFT mode with mutez transfer entry-point.


See also `fatoo list-contract-variants --help` for all available builds and details about them:


* `contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_contract`
    * `description`: The default.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `dbg_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_dbg_contract`
    * `description`: The default in debug mode.
    * `debug`: ✅
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `baby_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_baby_contract`
    * `description`: The default in Babylon mode.
    * `debug`: ⛌
    * `carthage_pairs`: ⛌
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `nolay_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_nolay_contract`
    * `description`: The default without right-combs.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ⛌
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `mutran_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_mutran_contract`
    * `description`: The default with mutez transfer entry-point.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `tokset_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_tokset_contract`
    * `description`: The default with non-consecutive token-IDs.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ⛌
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `perdesc_noops_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_perdesc_noops_contract`
    * `description`: The default without operators and with permissions-descriptor.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ⛌
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ✅
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `perdesc_noops_dbg_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_perdesc_noops_dbg_contract`
    * `description`: The perdesc_noops_contract but in debug mode.
    * `debug`: ✅
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ⛌
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ✅
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `single_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_single_contract`
    * `description`: The default for single-asset.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ✅
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `single_mutran_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_single_mutran_contract`
    * `description`: The single-asset with mutez transfer entry-point.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ✅
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `nft_mutran_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_nft_mutran_contract`
    * `description`: The default in NFT mode with mutez transfer entry-point.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ⛌
    * `non_fungible`: ✅
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ⛌
* `lzep_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_lzep_contract`
    * `description`: The default with lazy-entry-points flag.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ✅
    * `lazy_entry_points_multiple`: ⛌
* `lzepm_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_lzepm_contract`
    * `description`: The default with lazy-entry-points-multiple flag.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ✅
* `lzep_mutran_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_lzep_mutran_contract`
    * `description`: The default with mutez-transfer and lazy-entry-points flag.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ✅
    * `lazy_entry_points_multiple`: ⛌
* `lzepm_mutran_contract`:
    * `version_string`: `version_20200910_tzip_93e5415e_lzepm_mutran_contract`
    * `description`: The default with mutez-transfer and lazy-entry-points-multiple flag.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
    * `lazy_entry_points`: ⛌
    * `lazy_entry_points_multiple`: ✅
